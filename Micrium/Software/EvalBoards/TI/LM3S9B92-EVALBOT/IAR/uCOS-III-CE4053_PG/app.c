/*
*********************************************************************************************************
*                                              EXAMPLE CODE
*
*                          (c) Copyright 2009-2010; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
* Filename      : app.c
* Version       : V1.00
* Programmer(s) : FUZZI
*
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include <includes.h>
#include <stdio.h>
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"

#include "ext/SD_Card.h"

/*
*********************************************************************************************************
*                                             LOCAL DEFINES
*********************************************************************************************************
*/

#define ONESECONDTICK             7000000

#define LEDBLINKPERIOD                   5
#define MOVEFORWARDPERIOD                10
#define MOVEBACKWARDPERIOD               17
#define LEFTTURNPERIOD                   25
#define RIGHTTURNPERIOD                   47

#define WORKLOAD1                    1
#define WORKLOAD2                    1
#define WORKLOAD3                    1

#define TIMERDIV                      (BSP_CPUClkFreq() / (CPU_INT32U)OSCfg_TickRate_Hz)

#define MICROSD_EN                    0
#define TIMER_EN                      0

/*
*********************************************************************************************************
*                                            LOCAL VARIABLES
*********************************************************************************************************
*/

static  OS_TCB       AppTaskStartTCB;
static  CPU_STK      AppTaskStartStk[APP_TASK_START_STK_SIZE];

static  OS_TCB       LEDBlinkTCB;
static  CPU_STK      LEDBlinkStk[LED_BLINK_STK_SIZE];

static  OS_TCB       moveForwardTCB;
static  CPU_STK      moveForwardStk[MOVE_FORWARD_STK_SIZE];

static  OS_TCB       moveBackwardTCB;
static  CPU_STK      moveBackwardStk[MOVE_BACKWARD_STK_SIZE];

static  OS_TCB       leftTurnTCB;
static  CPU_STK      leftTurnStk[LEFT_TURN_STK_SIZE];

static  OS_TCB       rightTurnTCB;
static  CPU_STK      rightTurnStk[RIGHT_TURN_STK_SIZE];


CPU_INT32U      iCnt = 0;
CPU_INT08U      Left_tgt;
CPU_INT08U      Right_tgt;

CPU_INT32U      iToken  = 0;
CPU_INT32U      iCounter= 1;
CPU_INT32U      iMove   = 10;


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

        void        IntWheelSensor                    ();
static  void        AppRobotMotorDriveSensorEnable    ();
static  void        AppRobotMotorDriveSensorDisable   ();

        void        RoboTurn                          (tSide dir, CPU_INT16U seg, CPU_INT16U speed);

static  void        AppTaskStart                 (void  *p_arg);
static  void        AppTaskOne                   (void  *p_arg);
static  void        AppTaskTwo                   (void  *p_arg);
static  void        AppTaskThree                 (void  *p_arg);

static  void        LEDBlink                   (void *p_arg);
static  void        moveForward                   (void  *p_arg);
static  void        moveBackward                   (void  *p_arg);
static  void        leftTurn                   (void  *p_arg);
static  void        rightTurn                   (void  *p_arg);


#if(TIMER_EN == 1)
extern void TimerReset(void);
extern unsigned long TimerTick(void);
unsigned long iTick1, iTick2, iTick3,iTick4;
#endif

/*
*********************************************************************************************************
*                                                main()
*
* Description : This is the standard entry point for C code.  It is assumed that your code will call
*               main() once you have performed all necessary initialization.
*
* Arguments   : none
*
* Returns     : none
*********************************************************************************************************
*/

int  main (void)
{
    OS_ERR  err;
    BSP_IntDisAll();                                            /* Disable all interrupts.                              */
    OSInit(&err);                                               /* Init uC/OS-III.                                      */

    OSTaskCreate((OS_TCB     *)&AppTaskStartTCB,           /* Create the start task                                */
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR ) AppTaskStart,
                 (void       *) 0,
                 (OS_PRIO     ) APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStartStk[0],
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE / 10u,
                 (CPU_STK_SIZE) APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  ) 0u,
                 (OS_TICK     ) 0u,
                 (void       *) (CPU_INT32U) 0, 
                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR     *)&err);

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III). */
}


/*
*********************************************************************************************************
*                                          STARTUP TASK
*
* Description : This is an example of a startup task.  As mentioned in the book's text, you MUST
*               initialize the ticker only once multitasking has started.
*
* Arguments   : p_arg   is the argument passed to 'AppTaskStart()' by 'OSTaskCreate()'.
*
* Returns     : none
*
* Notes       : 1) The first line of code is used to prevent a compiler warning because 'p_arg' is not
*                  used.  The compiler should not generate any code for this statement.
*********************************************************************************************************
*/

static  void  AppTaskStart (void  *p_arg)
{
    CPU_INT32U  clk_freq;
    CPU_INT32U  ulPHYMR0;
    CPU_INT32U  cnts;
    OS_ERR      err;

   (void)&p_arg;

    BSP_Init();                                                 /* Initialize BSP functions                             */
    CPU_Init();                                                 /* Initialize the uC/CPU services                       */
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ETH);                  /* Enable and Reset the Ethernet Controller.            */
    SysCtlPeripheralReset(SYSCTL_PERIPH_ETH);
    ulPHYMR0 = EthernetPHYRead(ETH_BASE, PHY_MR0);              /* Power Down PHY                                       */
    EthernetPHYWrite(ETH_BASE, PHY_MR0, ulPHYMR0 | PHY_MR0_PWRDN);
    SysCtlPeripheralDeepSleepDisable(SYSCTL_PERIPH_ETH);
    clk_freq = BSP_CPUClkFreq();                                /* Determine SysTick reference freq.                    */
    cnts     = clk_freq / (CPU_INT32U)OSCfg_TickRate_Hz;        /* Determine nbr SysTick increments                     */
    OS_CPU_SysTickInit(cnts);                                   /* Init uC/OS periodic time src (SysTick).              */
    CPU_TS_TmrFreqSet(clk_freq);
    
    /* Enable Wheel ISR Interrupt */
    AppRobotMotorDriveSensorEnable();

    OSRecTaskCreate((OS_TCB     *)&LEDBlinkTCB, (CPU_CHAR   *)"LED Blink", 5u,(OS_TASK_PTR ) LEDBlink, (void       *) 0, (OS_PRIO     ) 11u, (CPU_STK    *)&LEDBlinkStk[0], (CPU_STK_SIZE) LED_BLINK_STK_SIZE / 10u, (CPU_STK_SIZE) LED_BLINK_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *)(CPU_INT32U) 2u, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSRecTaskCreate((OS_TCB     *)&moveForwardTCB, (CPU_CHAR   *)"Move Forward", 10u,(OS_TASK_PTR ) moveForward, (void       *) 0, (OS_PRIO     ) 12u, (CPU_STK    *)&moveForwardStk[0], (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE / 10u, (CPU_STK_SIZE) MOVE_FORWARD_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2u, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSRecTaskCreate((OS_TCB     *)&moveBackwardTCB, (CPU_CHAR   *)"Move Backward", 17u,(OS_TASK_PTR ) moveBackward, (void       *) 0, (OS_PRIO     ) 13u, (CPU_STK    *)&moveBackwardStk[0], (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE / 10u, (CPU_STK_SIZE) MOVE_BACKWARD_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2u, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSRecTaskCreate((OS_TCB     *)&leftTurnTCB, (CPU_CHAR   *)"Left Turn", 25u,(OS_TASK_PTR ) leftTurn, (void       *) 0, (OS_PRIO     ) 14u, (CPU_STK    *)&leftTurnStk[0], (CPU_STK_SIZE) LEFT_TURN_STK_SIZE / 10u, (CPU_STK_SIZE) LEFT_TURN_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2u, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
    OSRecTaskCreate((OS_TCB     *)&rightTurnTCB, (CPU_CHAR   *)"Right Turn", 47u,(OS_TASK_PTR ) rightTurn, (void       *) 0, (OS_PRIO     ) 15u, (CPU_STK    *)&rightTurnStk[0], (CPU_STK_SIZE) RIGHT_TURN_STK_SIZE / 10u, (CPU_STK_SIZE) RIGHT_TURN_STK_SIZE, (OS_MSG_QTY  ) 0u, (OS_TICK     ) 0u, (void       *) (CPU_INT32U) 2u, (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), (OS_ERR     *)&err);
 
    OSRecStart();
    /* Delete this task */
    OSTaskDel((OS_TCB *)0, &err);
}

static  void  LEDBlink (void *p_arg)
{   
  //printf("LEDBlink starts");
  OS_ERR      err;
  
  CPU_INT32U  k, i, j;
  BSP_LED_On(0u);      
  
  //OSTimeDly(500, OS_OPT_TIME_DLY, &err);
  for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2) + j);
	} 
//     for(k=0; k<WORKLOAD1; k++)
//    {
//      for(i=0; i <ONESECONDTICK/2; i++){
//        j=2*i;
//      }
//    }
    
  BSP_LED_Off(0u);
 
  //printf("LEDBlink finished");
  
  
/* Delete this task */

  OSTaskRecDel((OS_TCB *)0, &err);
            
}


static  void  moveForward (void  *p_arg)
{ 
  //printf("forward starts");

  OS_ERR      err;
  CPU_INT32U  k, i, j;
  
  RoboTurn(FRONT, 4, 80);
 //OSTimeDly(1000, OS_OPT_TIME_DLY, &err);
  
  for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2) + j);
	} 
//     for(k=0; k<WORKLOAD1; k++)
//    {
//      for(i=0; i <ONESECONDTICK/2; i++){
//        j=2*i;
//      }
//    }
   
     //printf("forward stops");

/* Delete this task */
    
	OSTaskRecDel((OS_TCB *)0, &err);
}


static  void moveBackward (void *p_arg)
{
  
  //printf("backward starts");
  OS_ERR      err;
  CPU_INT32U  k, i, j;
  
  RoboTurn(BACK,4, 80);
//OSTimeDly(1000, OS_OPT_TIME_DLY, &err);
   for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2) + j);
	} 
//     for(k=0; k<WORKLOAD1; k++)
//    {
//      for(i=0; i <ONESECONDTICK/2; i++){
//        j=2*i;
//      }
//    }
    //printf("backward stops");

/* Delete this task */
  
    OSTaskRecDel((OS_TCB *)0, &err);

}

static  void leftTurn (void *p_arg)
{
  //printf("left starts");
  OS_ERR      err;
  CPU_INT32U  k, i, j;
  
  RoboTurn(LEFT_SIDE, 8, 80);
//OSTimeDly(1000, OS_OPT_TIME_DLY, &err);
  
   for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2) + j);
	} 
//     for(k=0; k<WORKLOAD1; k++)
//    {
//      for(i=0; i <ONESECONDTICK/2; i++){
//        j=2*i;
//      }
//    }
     //printf("left finished");

/* Delete this task */
  
	OSTaskRecDel((OS_TCB *)0, &err);
  
}

static  void rightTurn (void *p_arg)
{
    //printf("right starts");

  OS_ERR      err;
  CPU_INT32U  k, i, j;
  
  RoboTurn(RIGHT_SIDE, 8, 80);
   //OSTimeDly(1000, OS_OPT_TIME_DLY, &err);
  
  for(i=0; i <ONESECONDTICK; i++){
         j = ((i * 2) + j);
	} 
//     for(k=0; k<WORKLOAD1; k++)
//    {
//      for(i=0; i <ONESECONDTICK/2; i++){
//        j=2*i;
//      }
//    }
   
    // printf("right finished");

/* Delete this task */
   
	OSTaskRecDel((OS_TCB *)0, &err);
  
}

void IntWheelSensor()
{
	CPU_INT32U         ulStatusR_A;
	CPU_INT32U         ulStatusL_A;

	static CPU_INT08U CountL = 0;
	static CPU_INT08U CountR = 0;

	static CPU_INT08U data = 0;

	ulStatusR_A = GPIOPinIntStatus(RIGHT_IR_SENSOR_A_PORT, DEF_TRUE);
	ulStatusL_A = GPIOPinIntStatus(LEFT_IR_SENSOR_A_PORT, DEF_TRUE);

        if (ulStatusR_A & RIGHT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(RIGHT_IR_SENSOR_A_PORT, RIGHT_IR_SENSOR_A_PIN);           /* Clear interrupt.*/
          CountR = CountR + 1;
        }

        if (ulStatusL_A & LEFT_IR_SENSOR_A_PIN)
        {
          GPIOPinIntClear(LEFT_IR_SENSOR_A_PORT, LEFT_IR_SENSOR_A_PIN);
          CountL = CountL + 1;
        }

	if((CountL >= Left_tgt) && (CountR >= Right_tgt))
        {
          data = 0x11;
          Left_tgt = 0;
          Right_tgt = 0;
          CountL = 0;
          CountR = 0;
          BSP_MotorStop(LEFT_SIDE);
          BSP_MotorStop(RIGHT_SIDE);
        }
        else if(CountL >= Left_tgt)
        {
          data = 0x10;
          Left_tgt = 0;
          CountL = 0;
          BSP_MotorStop(LEFT_SIDE);
        }
        else if(CountR >= Right_tgt)
        {
          data = 0x01;
          Right_tgt = 0;
          CountR = 0;
          BSP_MotorStop(RIGHT_SIDE);
        }
        return;
}


static  void  AppRobotMotorDriveSensorEnable ()
{
    BSP_WheelSensorEnable();
    BSP_WheelSensorIntEnable(RIGHT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
    BSP_WheelSensorIntEnable(LEFT_SIDE, SENSOR_A, (CPU_FNCT_VOID)IntWheelSensor);
}

static  void  AppRobotMotorDriveSensorDisable ()
{
    BSP_WheelSensorDisable();
    BSP_WheelSensorIntDisable(RIGHT_SIDE, SENSOR_A);
    BSP_WheelSensorIntDisable(LEFT_SIDE, SENSOR_A);
}

void RoboTurn(tSide dir, CPU_INT16U seg, CPU_INT16U speed)
{
	Left_tgt = seg;
        Right_tgt = seg;

	BSP_MotorStop(LEFT_SIDE);
	BSP_MotorStop(RIGHT_SIDE);

        BSP_MotorSpeed(LEFT_SIDE, speed <<8u);
	BSP_MotorSpeed(RIGHT_SIDE,speed <<8u);

	switch(dir)
	{
            case FRONT :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case BACK :
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            case LEFT_SIDE :
                    BSP_MotorDir(RIGHT_SIDE,FORWARD);
                    BSP_MotorDir(LEFT_SIDE,REVERSE);
                    BSP_MotorRun(LEFT_SIDE);
                    BSP_MotorRun(RIGHT_SIDE);
                    break;
                    
            case RIGHT_SIDE:
                    BSP_MotorDir(LEFT_SIDE,FORWARD);
                    BSP_MotorDir(RIGHT_SIDE,REVERSE);
                    BSP_MotorRun(RIGHT_SIDE);
                    BSP_MotorRun(LEFT_SIDE);
                    break;
                    
            default:
                    BSP_MotorStop(LEFT_SIDE);
                    BSP_MotorStop(RIGHT_SIDE);
                    break;
	}

	return;
}

#if(TIMER_EN == 1)
unsigned long TimerTick(void)
{
    return((TimerValueGet(TIMER3_BASE, TIMER_A)/TIMERDIV));
}

void TimerReset(void)
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER3);
    SysCtlPeripheralReset(SYSCTL_PERIPH_TIMER3);
    TimerConfigure(TIMER3_BASE, TIMER_CFG_32_BIT_PER_UP);
    TimerLoadSet(TIMER3_BASE, TIMER_BOTH, 0xffffffff);
    TimerEnable(TIMER3_BASE, TIMER_BOTH);
}
#endif