#include <OS_RecTask.h>
#include <OS_ResourceUtil.h>
#include <os.h> 
 
/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
OS_TCB OSRecRlsTaskTCB;
OS_RBTree OSRecTaskTree;
OS_MEM *RecPartitionPtr;
OS_MEM *RecListPartitionPtr;
CPU_STK OSCfg_RecRlsTaskStk[OS_CFG_REC_RLS_TASK_STK_SIZE];
CPU_STK * const  OSCfg_RecRlsTaskStkBasePtr = (CPU_STK *)&OSCfg_RecRlsTaskStk[0];
CPU_STK_SIZE const  OSCfg_RecRlsTaskStkLimit = (CPU_STK_SIZE)OS_CFG_REC_RLS_TASK_STK_LIMIT;
CPU_STK_SIZE const  OSCfg_RecRlsTaskStkSize = (CPU_STK_SIZE)OS_CFG_REC_RLS_TASK_STK_SIZE;
int OverheadValue, StartTime, StartTime2;
/*
************************************************************************************************************************
*                                                    CREATE A RECURSIVE TASK
*
* Description: This function is used to have the OS manage the execution of a recursive task.  
*
* Arguments  : p_tcb          is a pointer to the task's TCB
*
*              p_name         is a pointer to an ASCII string to provide a name to the task.
*
*              period         is the period of the recursive task to be created         
*
*              p_task         is a pointer to the task's code
*
*              p_arg          is a pointer to an optional data area which can be used to pass parameters to
*                             the task when the task first executes.  Where the task is concerned it thinks
*                             it was invoked and passed the argument 'p_arg' as follows:
*
*                                 void Task (void *p_arg)
*                                 {
*                                     for (;;) {
*                                         Task code;
*                                     }
*                                 }
*
*              prio           is the task's priority.  A unique priority MUST be assigned to each task and the
*                             lower the number, the higher the priority.
*
*              p_stk_base     is a pointer to the base address of the stack (i.e. low address).
*
*              stk_limit      is the number of stack elements to set as 'watermark' limit for the stack.  This value
*                             represents the number of CPU_STK entries left before the stack is full.  For example,
*                             specifying 10% of the 'stk_size' value indicates that the stack limit will be reached
*                             when the stack reaches 90% full.
*
*              stk_size       is the size of the stack in number of elements.  If CPU_STK is set to CPU_INT08U,
*                             'stk_size' corresponds to the number of bytes available.  If CPU_STK is set to
*                             CPU_INT16U, 'stk_size' contains the number of 16-bit entries available.  Finally, if
*                             CPU_STK is set to CPU_INT32U, 'stk_size' contains the number of 32-bit entries
*                             available on the stack.
*
*              q_size         is the maximum number of messages that can be sent to the task
*
*              time_quanta    amount of time (in ticks) for time slice when round-robin between tasks.  Specify 0 to use
*                             the default.
*
*              p_ext          is a pointer to a user supplied memory location which is used as a TCB extension.
*                             For example, this user memory can hold the contents of floating-point registers
*                             during a context switch, the time each task takes to execute, the number of times
*                             the task has been switched-in, etc.
*
*              opt            contains additional information (or options) about the behavior of the task.
*                             See OS_OPT_TASK_xxx in OS.H.  Current choices are:
*
*                                 OS_OPT_TASK_NONE            No option selected
*                                 OS_OPT_TASK_STK_CHK         Stack checking to be allowed for the task
*                                 OS_OPT_TASK_STK_CLR         Clear the stack when the task is created
*                                 OS_OPT_TASK_SAVE_FP         If the CPU has floating-point registers, save them
*                                                             during a context switch.
*
*              p_err          is a pointer to an error code that will be set during this call.  The value pointer
*                             to by 'p_err' can be:
*
*                                 OS_ERR_NONE                    if the function was successful.
*                                 OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the task after you called
*                                                                   OSSafetyCriticalStart().
*                                 OS_ERR_NAME                    if 'p_name' is a NULL pointer
*                                 OS_ERR_PRIO_INVALID            if the priority you specify is higher that the maximum
*                                                                   allowed (i.e. >= OS_CFG_PRIO_MAX-1) or,
*                                                                if OS_CFG_ISR_POST_DEFERRED_EN is set to 1 and you tried
*                                                                   to use priority 0 which is reserved.
*                                 OS_ERR_STK_INVALID             if you specified a NULL pointer for 'p_stk_base'
*                                 OS_ERR_STK_SIZE_INVALID        if you specified zero for the 'stk_size'
*                                 OS_ERR_STK_LIMIT_INVALID       if you specified a 'stk_limit' greater than or equal
*                                                                   to 'stk_size'
*                                 OS_ERR_TASK_CREATE_ISR         if you tried to create a task from an ISR.
*                                 OS_ERR_TASK_INVALID            if you specified a NULL pointer for 'p_task'
*                                 OS_ERR_TCB_INVALID             if you specified a NULL pointer for 'p_tcb'
*
* Returns    : A pointer to the TCB of the task created.  This pointer must be used as an ID (i.e handle) to the task.
************************************************************************************************************************
*/
void OSRecTaskCreate(OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    int	           period,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err){
	CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();
    


#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb -> resourceCount = 0;
    p_tcb -> originalPrio = prio;
//    printf("ori = %d\n", p_tcb -> originalPrio);
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();
 /*   OS_PrioInsert(p_tcb->Prio);
    OS_RdyListInsertTail(p_tcb);*/ /* We don't allow the task to enter the ready list*/

    /*Insert the task into the task tree*/
    if(period != 0)
      OSRecTaskTreeInsert(period, p_tcb);
    /*Insert the task into the ready structure if the task is created after OSRecStart() is called*/
    if(schedEnable == 1)
      heap_insert(&OSRdyMinHeap, period, p_tcb);
    
#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();
}

/*
************************************************************************************************************************
*                                                    RECREATE A RECURSIVE TASK
*
* Description: This function is used to recreate an existing task that has been created for
*              its next instance's execution.  
*
* Arguments  : p_tcb          is a pointer to the task's TCB
*
*              p_name         is a pointer to an ASCII string to provide a name to the task.
*
*              period         is the period of the recursive task to be created         
*
*              p_task         is a pointer to the task's code
*
*              p_arg          is a pointer to an optional data area which can be used to pass parameters to
*                             the task when the task first executes.  Where the task is concerned it thinks
*                             it was invoked and passed the argument 'p_arg' as follows:
*
*                                 void Task (void *p_arg)
*                                 {
*                                     for (;;) {
*                                         Task code;
*                                     }
*                                 }
*
*              prio           is the task's priority.  A unique priority MUST be assigned to each task and the
*                             lower the number, the higher the priority.
*
*              p_stk_base     is a pointer to the base address of the stack (i.e. low address).
*
*              stk_limit      is the number of stack elements to set as 'watermark' limit for the stack.  This value
*                             represents the number of CPU_STK entries left before the stack is full.  For example,
*                             specifying 10% of the 'stk_size' value indicates that the stack limit will be reached
*                             when the stack reaches 90% full.
*
*              stk_size       is the size of the stack in number of elements.  If CPU_STK is set to CPU_INT08U,
*                             'stk_size' corresponds to the number of bytes available.  If CPU_STK is set to
*                             CPU_INT16U, 'stk_size' contains the number of 16-bit entries available.  Finally, if
*                             CPU_STK is set to CPU_INT32U, 'stk_size' contains the number of 32-bit entries
*                             available on the stack.
*
*              q_size         is the maximum number of messages that can be sent to the task
*
*              time_quanta    amount of time (in ticks) for time slice when round-robin between tasks.  Specify 0 to use
*                             the default.
*
*              p_ext          is a pointer to a user supplied memory location which is used as a TCB extension.
*                             For example, this user memory can hold the contents of floating-point registers
*                             during a context switch, the time each task takes to execute, the number of times
*                             the task has been switched-in, etc.
*
*              opt            contains additional information (or options) about the behavior of the task.
*                             See OS_OPT_TASK_xxx in OS.H.  Current choices are:
*
*                                 OS_OPT_TASK_NONE            No option selected
*                                 OS_OPT_TASK_STK_CHK         Stack checking to be allowed for the task
*                                 OS_OPT_TASK_STK_CLR         Clear the stack when the task is created
*                                 OS_OPT_TASK_SAVE_FP         If the CPU has floating-point registers, save them
*                                                             during a context switch.
*
*              p_err          is a pointer to an error code that will be set during this call.  The value pointer
*                             to by 'p_err' can be:
*
*                                 OS_ERR_NONE                    if the function was successful.
*                                 OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the task after you called
*                                                                   OSSafetyCriticalStart().
*                                 OS_ERR_NAME                    if 'p_name' is a NULL pointer
*                                 OS_ERR_PRIO_INVALID            if the priority you specify is higher that the maximum
*                                                                   allowed (i.e. >= OS_CFG_PRIO_MAX-1) or,
*                                                                if OS_CFG_ISR_POST_DEFERRED_EN is set to 1 and you tried
*                                                                   to use priority 0 which is reserved.
*                                 OS_ERR_STK_INVALID             if you specified a NULL pointer for 'p_stk_base'
*                                 OS_ERR_STK_SIZE_INVALID        if you specified zero for the 'stk_size'
*                                 OS_ERR_STK_LIMIT_INVALID       if you specified a 'stk_limit' greater than or equal
*                                                                   to 'stk_size'
*                                 OS_ERR_TASK_CREATE_ISR         if you tried to create a task from an ISR.
*                                 OS_ERR_TASK_INVALID            if you specified a NULL pointer for 'p_task'
*                                 OS_ERR_TCB_INVALID             if you specified a NULL pointer for 'p_tcb'
*
* Returns    : A pointer to the TCB of the task created.  This pointer must be used as an ID (i.e handle) to the task.
************************************************************************************************************************
*/
void OSRecTaskRecreate(OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err){
	CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb -> resourceCount = 0;
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */

    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */

                                                            /* --------------- ADD TASK TO READY LIST --------------- */
    OS_CRITICAL_ENTER();
 /*   OS_PrioInsert(p_tcb->Prio);
    OS_RdyListInsertTail(p_tcb);*/ /* We don't allow the task to enter the ready list*/

    /*Insert the task into the task tree*/
//    OSRecTaskTreeInsert(period, p_tcb);

#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListAdd(p_tcb);
#endif

    OSTaskQty++;                                            /* Increment the #tasks counter                           */

    if (OSRunning != OS_STATE_OS_RUNNING) {                 /* Return if multitasking has not started                 */
        OS_CRITICAL_EXIT();
        return;
    }

    OS_CRITICAL_EXIT_NO_SCHED();

    OSSched();
}

/*
*********************************************************************************************************
*                                            DELETE A RECURSIVE TASK
*
* Description : This function allows you to delete a recursive task. Deleting a recursive task
*               removes the task from the ready structure so that it will no longer be considered
*               for scheduling.        
*
* Arguments   : p_tcb     is the TCB of the task to be deleted
*
*              p_err      is a pointer to an error code returned by this function.
*
* Returns     : none
*********************************************************************************************************
*/
void  OSTaskRecDel (OS_TCB  *p_tcb,
                 OS_ERR  *p_err){
                   int i;
      CPU_SR_ALLOC();
      OS_CRITICAL_ENTER();
      OSSchedLock (p_err);
      
//      OSTaskDel(p_tcb, p_err);
     
      deleteNode(&OSRdyMinHeap);

      OSSchedUnlock (p_err);
      OS_CRITICAL_EXIT();
 }

/*
*********************************************************************************************************
*                                            START RECURSIVE TASK MANAGEMENT
*
* Description : This function allows you to start the recursive task management. This function
*               should be called when you have created all the recursive tasks that want to be
*               released synchronously at time 0.        
*
* Arguments   : none 
*
* Returns     : none
*********************************************************************************************************
*/
void OSRecStart(){

  OS_ERR  *p_err;
  rb_node* root = OSRecTaskTree.root;

  tickCtr = 0;
  secondCtr = 0;
 
  OSTaskCreate((OS_TCB     *)&OSRecRlsTaskTCB,
             (CPU_CHAR   *)((void *)"uC/OS-III Rec Release Task"),
             (OS_TASK_PTR )OS_RecRlsTask,
             (void       *)0,
             (OS_PRIO     )OS_CFG_REC_RLS_TASK_PRIO,
             (CPU_STK    *)OSCfg_RecRlsTaskStkBasePtr,
             (CPU_STK_SIZE)OSCfg_RecRlsTaskStkLimit,
             (CPU_STK_SIZE)OSCfg_RecRlsTaskStkSize,
             (OS_MSG_QTY  )0u,
             (OS_TICK     )0u,
             (void       *)0,
             (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
             (OS_ERR     *)p_err);
  
  pushTasksToReady(root);
  CPU_SR_ALLOC();
  OS_CRITICAL_ENTER();     
  schedEnable = 1;
  OS_CRITICAL_EXIT();

}

//in order traversal is used 
void printTreee(rb_node* root){
  if(root == NULL){
    return;
  }
  //printf("period = %d\n", root->key);
  printTreee(root -> left);       
  printTreee(root -> right);
}

/*
*********************************************************************************************************
*                                            PUSH TASK TO THE READY STRUCTURE
*
* Description : This function push all the tasks that have been pushed into the tree into 
*               the ready structure.        
*
* Arguments   : root    is the root of OS_RBTree whose tasks are to be pushed to ready structure 
*
* Returns     : none
*********************************************************************************************************
*/
//in order traversal is used 
void pushTasksToReady(rb_node* root){
  if(root == NULL){
    return;
  }
  pushTasksToReady(root -> left);       
  pushTaskToReady(root);
//  heap_insert(&OSRdyMinHeap, root -> period, root -> value);
  pushTasksToReady(root -> right);
}

/*
*********************************************************************************************************
*                                            PUSH A TASK TO THE READY STRUCTURE
*
* Description : This function push all the tasks that is in the linked list contained by
*               the given node.        
*
* Arguments   : node    is the rb_node whose tasks are to be pushed to ready structure 
*
* Returns     : none
*********************************************************************************************************
*/
void pushTaskToReady(rb_node* node){
  //push task to ready, with the key value of the node's period
  int i;
  rb_list_node* cur = node->ll.head;
  for(i = 0;i<=node->ll.size-1;i++){
    heap_insert(&OSRdyMinHeap, cur -> period, cur -> value);

    cur = cur -> next;
  }

}

/*
*********************************************************************************************************
*                                            INITIALIZE RECURSIVE TASK MANAGEMENENT
*
* Description : This function performs all the necessary initializations for the recursive
*               task management.        
*
* Arguments   : p_err      is a pointer to an error code returned by this function.
*
* Returns     : none
*********************************************************************************************************
*/
void OS_RecTaskInit(OS_ERR  *p_err){
    createRecPartition();
    createSchedPartition();
    createRecListPartition();
    createSchedListPartition();
    OSRecTaskTree.min = 0;
    OSRdyMinHeap.size = 0;
//    OSTaskCreate((OS_TCB     *)&OSRecRlsTaskTCB,
//                 (CPU_CHAR   *)((void *)"uC/OS-III Rec Release Task"),
//                 (OS_TASK_PTR )OS_RecRlsTask,
//                 (void       *)0,
//                 (OS_PRIO     )OS_CFG_REC_RLS_TASK_PRIO,
//                 (CPU_STK    *)OSCfg_RecRlsTaskStkBasePtr,
//                 (CPU_STK_SIZE)OSCfg_RecRlsTaskStkLimit,
//                 (CPU_STK_SIZE)OSCfg_RecRlsTaskStkSize,
//                 (OS_MSG_QTY  )0u,
//                 (OS_TICK     )0u,
//                 (void       *)0,
//                 (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
//                 (OS_ERR     *)p_err);
}

/*
*********************************************************************************************************
*                                            RECURSIVE RELEASE OF TASK AND UPDATE THE RECURSIVE STRUCTURE
*
* Description : This function checks the minimum key in the recursive structure and release some
*               task(s) if necessary into the ready structure. In such case, the task will be
*               reinserted into the recursive structure with a new key value.
*
* Arguments   : p_err      is a pointer to an error code returned by this function.
*
* Returns     : none
*********************************************************************************************************
*/
void OS_RecTreeUpdate(){
  OS_ERR  err;
  int i, j, llsize;
  CPU_SR_ALLOC();
  OS_CRITICAL_ENTER();
  OSSchedLock (&err);
  tickCtr++;

  OSSchedUnlock (&err);
  OS_CRITICAL_EXIT();
  if(tickCtr == OS_CFG_TICK_RATE_HZ){

    secondCtr++;
    tickCtr = 0;
  }
  else 
    return;
  //push the task with the minimum next release time to the ready heap is necessary
                                       
  if(secondCtr == OSRecTaskTree.min){
    //counter value matches minimum release time in tree
    //remove the task(s) with minimum release time from tree

    rb_node* minTreeNode = rb_tree_remove_min(&OSRecTaskTree);  
 
    treeMinSearch(OSRecTaskTree.root, &(OSRecTaskTree.min));
          
    llsize = minTreeNode->ll.size;
    
    for(j = 0; j <= llsize-1;j++){
            rb_list_node* minListNode = removeNode(&(minTreeNode->ll), 0);
            OS_TCB* minTCB = minListNode -> value;
            
            OSRecTaskRecreate(minListNode -> value,
                            minTCB -> NamePtr,
                            minTCB -> TaskEntryAddr,
                            minTCB -> TaskEntryArg,
                            minTCB -> Prio,
                            minTCB -> StkBasePtr,
                            (CPU_STK_SIZE) minTCB -> StkSize/ 10u,
                            minTCB -> StkSize,(OS_MSG_QTY) 0u, 
                            (OS_TICK) 0u, 
                            (void*)(CPU_INT32U) 1, 
                            (OS_OPT      )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), 
                            (OS_ERR     *)&err);
            //push the node to the ready heap
            
            //release time
          CPU_SR_ALLOC();
          OS_CRITICAL_ENTER();
          OSSchedLock (&err);
          heap_insert(&OSRdyMinHeap, minListNode -> period, minListNode -> value);
          OSSchedUnlock (&err);
          OS_CRITICAL_EXIT();

          //update the key value of the minimum node
          minListNode -> key = minListNode -> key + minListNode -> period;
          //reinsert the node to the tree again with the new key
          rb_tree_reinsert(&OSRecTaskTree, minListNode);
    }
    release_rb_node(minTreeNode);    

  }
}

/*
************************************************************************************************************************
*                                                      RECURSIVE RELEASE TASK
*
* Description: This task is to manage the recursive/period release of the created recursive tasks.
*
* Arguments  : p_arg     is an argument passed to the task when the task is created (unused).
*
* Returns    : none
************************************************************************************************************************
*/
void OS_RecRlsTask(void *p_arg){
    OS_ERR  err;
    CPU_TS  ts;

    p_arg = p_arg;               
    while (DEF_ON) {                 
        (void)OSTaskSemPend((OS_TICK  )0,
                            (OS_OPT   )OS_OPT_PEND_BLOCKING,
                            (CPU_TS  *)&ts,
                            (OS_ERR  *)&err);
         if (err == OS_ERR_NONE) {
            if (OSRunning == OS_STATE_OS_RUNNING) {
                OS_RecTreeUpdate();                                              
            }
        }               /* Wait for signal from tick interrupt                    */
    }
}