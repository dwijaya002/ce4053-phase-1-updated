#include <OS_Task_Scheduling.h>

/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
OS_MinHeap OSRdyMinHeap;
OS_MEM *SchedPartitionPtr, *SchedListPartitionPtr;
int tickCtr, secondCtr;
//int flagl = 0;
int schedEnable = 0;

/*
************************************************************************************************************************
*                                                      CREATE PARTITION FOR SCHEDULING MANAGEMENT
*
* Description: This function create a memory partition to maintain the data structure used
*              for the scheduling of the recursive tasks.
*
* Arguments  : none
*
* Returns    : none
************************************************************************************************************************
*/
void createSchedPartition(){
    int block_size = sizeof(heapNode);
    void *p_storage; //= malloc(MEM_QTY * block_size); 
    OS_ERR err;
    SchedPartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (SchedPartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) SchedPartitionPtr, 
        (CPU_CHAR *) "Sched Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

void createSchedListPartition(){
    int block_size = sizeof(heap_list_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    SchedListPartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (SchedListPartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) SchedListPartitionPtr, 
        (CPU_CHAR *) "Sched List Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

/*
************************************************************************************************************************
*                                               INSERT A TASK TO READY HEAP       
*
* Description: This function inserts a task's TCB into the given ready heap based on the given key.
*
* Arguments  : heap     is a pointer to the ready heap to which the task is going to be inserted to
*              key      is the key used for the insertion of the tast, in this case
*                       which is using RM scheduling algo, the key is the period of the recursive task
*              item     is the TCB of the recursive task to be inserted
*
*
* Returns    : none
************************************************************************************************************************
*/
void heap_insert(OS_MinHeap* heap, int key, OS_TCB* item){
	int i, j, parent;
        heapNode *item_addr = heap -> items, *parent_addr;
        heapNode *new_item_addr = (heap -> items)+heap->size;
        heapNode *root = heap -> items;
	heapNode *newNode;
        OS_TCB* a;

        for(j = 0 ; j <= (heap->size)-1 ;j++){
          if(heap->items[j].key == key){
 
            insertHeapListNode(&(heap->items[j].ll), heap->items[j].ll.size, item);
            return;
          }
        }
//        printf("insert to heap\n");
        
        newNode = createNode(key);//, item);
        
//        //printf("size = %d\n", heap -> size);
//        //printf("new key = %d\n", key);
        if(heap -> size == 0){
          heap -> items = newNode;
          OS_PrioInsert(12u);
        }
 
        i = (heap -> size)++;

        while(i && (key < heap ->items[PARENT(i)].key)){
          heap->items[i].key = heap->items[PARENT(i)].key;
          heap->items[i].ll = heap->items[PARENT(i)].ll;
          i = PARENT(i);
        }
        
        heap->items[i].key = key;
        heap->items[i].ll.size = 0;
        heap->items[i].ll.head = NULL;
        heap->items[i].ll.tail = NULL;

        insertHeapListNode(&(heap->items[i].ll), 0, item); 
        a = heap->items[i].ll.head->value;

}

/*
************************************************************************************************************************
*                                               CREATE A NEW HEAP NODE      
*
* Description: This function create a new heap node with the given key and value.
*
* Arguments  : key      is the key for the new heap node
*              item     is the value for the new heap node
*
*
* Returns    : A pointer to the new created heap node
************************************************************************************************************************
*/
heapNode* createNode(int key){//, OS_TCB* item){
	//heapNode* newNode = malloc(sizeof(heapNode));
        OS_ERR err;
        heapNode* newNode = (heapNode *)OSMemGet((OS_MEM *)SchedPartitionPtr, 
                            (OS_ERR *)&err);
	newNode -> key = key; 
        newNode->ll.size = 0;
        newNode->ll.head = NULL;
        newNode->ll.tail = NULL;
        
//        newNode -> value = item;
//        if(newNode == NULL)
//          //printf("NULL\n");
//        else
//          //printf("\nNOT NULL, newNode period = %d\n", newNode -> key);
	return newNode;
}

/*
************************************************************************************************************************
*                                               SWAP 2 HEAP NODES      
*
* Description: This function swaps the key and value of the given heap nodes.
*
* Arguments  : node1      is a pointer to the first node to be swapped
*              node2      is a pointer to the second node to be swapped
*
*
* Returns    : none
************************************************************************************************************************
*/
void swap(heapNode* node1, heapNode* node2){
//	heapNode temp = *node1;
//	*node1 = *node2;
//	*node2 = temp;
      int tempKey;
      heap_linkedlist tempValue;
      
      tempKey = node1->key;
      tempValue = node1->ll;
      
      node1->key = node2->key;
      node1->ll = node2->ll;
      
      node2->key = tempKey;
      node2->ll = tempValue;
}

/*
************************************************************************************************************************
*                                               HEAPIFY HEAP    
*
* Description: This function repairs the given heap so that it satisfies the min-heap property.
*
* Arguments  : heap       is a pointer to the heap to be repaired
*              i          is the current level of the heap 
*
*
* Returns    : none
************************************************************************************************************************
*/
void heapify(OS_MinHeap* heap, int i) {
    int size = heap -> size;
    int leftChild =  2 * i + 1;
    int rightChild =  2 * i + 2;
    
    int leftKey = heap->items[leftChild].key;
    int rightKey = heap->items[rightChild].key;
    int currentKey = heap->items[i].key;
    
    int smallest = (leftChild < size && leftKey < currentKey) ? leftChild : i;

    if(rightChild < size && rightKey < heap->items[smallest].key) {
        smallest = rightChild;
    }
    if(smallest != i) {
        swap(&(heap->items[i]), &(heap->items[smallest]));
        heapify(heap, smallest);
    }
}

void heapifyUp(OS_MinHeap* heap, int k){
  int parent; 
  while (k != 0) { 
     /* Parent is not the root */
     parent = PARENT(k);
     if (heap->items[k].key < heap->items[parent].key){ 
//        help = a[parent];
//        a[parent] = a[k];
//        a[k] = help;
        swap(&(heap->items[k]), &(heap->items[parent]));
        
        k = parent;          // k moved up one level
     }
     else{
        break;
     }

  }
}
/*
************************************************************************************************************************
*                                               DELETE MINIMUM NODE FROM THE HEAP    
*
* Description: This function deletes the minimum node from the given heap.
*
* Arguments  : heap       is a pointer to the heap whose minimum node is to be deleted
*
*
* Returns    : The pointer to the minimum heap node of the given heap
************************************************************************************************************************
*/
heapNode* deleteNode(OS_MinHeap *heap) {
    heapNode *min = NULL, *last;
    OS_ERR err;
    int i;
    //heap is not empty
    if(heap -> size > 0) {
        removeHeapListNode(&(heap -> items[0].ll), 0);
        if(heap -> items[0].ll.size >= 1){
          return &(heap -> items[0]);
        }
        
        min = &(heap -> items[0]);
        --(heap->size);
        heap->items[0].key = heap->items[heap->size].key ;
        heap->items[0].ll = heap->items[heap->size].ll;
        last = (heap -> items) + heap->size;
        OSMemPut((OS_MEM *) SchedPartitionPtr, 
                  (void *) last, 
                  (OS_ERR *) &err);
 
        if(heap -> size == 0){
//          CPU_SR_ALLOC();
//          OS_CRITICAL_ENTER();
//          OSSchedLock (&err);

          heap -> items = NULL;
          OS_PrioRemove(12u);
//          OSSchedUnlock (&err);
//          OS_CRITICAL_EXIT();
        }
        else 
          heapify(heap, 0);

        return min;
    } 
   
    else {
        return NULL;
    }
}
  
void removeTCBHeap(OS_MinHeap *heap, int key, OS_TCB* p_tcb) {
    heapNode *minNode = NULL, *lastNode;
    OS_ERR err;
    
    int i;
    
    CPU_SR_ALLOC();
    OS_CRITICAL_ENTER();
    OSSchedLock (&err);
   
    for(i = 0 ; i <= heap -> size ; i++){
      if(key == heap->items[i].key){
        removeHeapListByValue(&(heap->items[i].ll), p_tcb);

        break;
      }
    }
    
    OSSchedUnlock (&err);
    OS_CRITICAL_EXIT();
}

/*
************************************************************************************************************************
*                                               GET MINIMUM NODE FROM THE HEAP    
*
* Description: This function gets the minimum node from the given heap.
*
* Arguments  : heap       is a pointer to the heap whose minimum node is to be fetched
*
*
* Returns    : The pointer to the TCB with the highest priority or lowest period
************************************************************************************************************************
*/
OS_TCB* getMinNode(OS_MinHeap *heap) {
    OS_TCB *minListTCB = NULL;
    //heap is not empty
    if(heap -> size > 0) {
        
        minListTCB = heap->items[0].ll.head->value;

        return minListTCB;
    } 
    //heap is empty
    else {
        //printf("HEAP NULL\n");
        return NULL;
    }
}         

heap_list_node* insertHeapListNode(heap_linkedlist *ll, int index, OS_TCB* value){
   OS_ERR err;  
   heap_list_node *pre, *cur;
   
   if (ll == NULL || index < 0 || index > ll->size + 1)
      return NULL;
   
   // If empty list or inserting first node, need to update head pointer
   if (ll->head == NULL || index == 0){
      cur = ll->head;
      ll->head = (heap_list_node *)OSMemGet((OS_MEM *)SchedListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = ll->head;

      ll->head->value = value;
      ll->head->next = cur;
      ll->size++;
      return ll->head;
   }
   
   // Inserting as new last node
   if (index == ll->size){
      pre = ll->tail;
      cur = pre->next;
      pre->next = (heap_list_node *)OSMemGet((OS_MEM *)SchedListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = pre->next;
      
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   // Find the nodes before and at the target position
   // Create a new node and reconnect the links
   if ((pre = findHeapListNode(ll, index-1)) != NULL){
      cur = pre->next;
      pre->next = (heap_list_node *)OSMemGet((OS_MEM *)SchedListPartitionPtr, 
                        (OS_ERR *)&err);
      
      if (index == ll->size)
         ll->tail = pre->next;
      
//      pre->next->key = period;
//      pre->next->period = period;
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   return NULL;
}

heap_list_node* findHeapListNode(heap_linkedlist *ll, int index){
   
   heap_list_node *temp;
   
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   temp = ll->head;
   
   if (temp == NULL || index < 0)
      return NULL;
   
   while (index > 0){
      temp = temp->next;
      if (temp == NULL)
         return NULL;
      index--;
   }
   
   return temp;
}

heap_list_node* removeHeapListNode(heap_linkedlist *ll, int index){
   OS_ERR err;  
   heap_list_node *pre, *cur, *minListNode = NULL;
   
   // Highest index we can remove is size-1
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   // If removing first node, need to update head pointer
   if (index == 0){
      minListNode = ll->head;
      OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                (void *) minListNode, 
                (OS_ERR *) &err);
      cur = ll->head->next;
      ll->head = cur;
      ll->size--;
      
      if (ll->size == 0)
         ll->tail = 0;
      
      return minListNode;
   }
   
   // Find the nodes before and after the target position
   // Free the target node and reconnect the links
   if ((pre = findHeapListNode(ll, index-1)) != NULL){
      
      // Removing the last node, update the tail pointer
      if (index == ll->size - 1){
         minListNode = pre -> next; 
         OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                  (void *) minListNode, 
                  (OS_ERR *) &err);
         ll->tail = pre;
         pre->next = NULL;
      }
      else{
         minListNode = pre->next;
         OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                    (void *) minListNode, 
                    (OS_ERR *) &err);
         cur = pre->next->next;
         pre->next = cur;
      }
      ll->size--;
      return minListNode;
   }
   
   return NULL;
}

void removeHeapListByValue(heap_linkedlist *ll, OS_TCB* p_tcb){
  int i;
  OS_ERR err;  
  int size = ll -> size;
  heap_list_node* cur = ll -> head;
  heap_list_node* pre;
  heap_list_node* minNode;
  for(i = 0 ; i <= size-1 ;i++){
    if(cur -> value == p_tcb){
      if(i == 0){
        minNode = ll->head;
        OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                  (void *) minNode, 
                  (OS_ERR *) &err);
        cur = ll->head->next;
        ll->head = cur;
        ll->size--;
        
        if (ll->size == 0)
           ll->tail = 0;
      }
      else{
        if (i == ll->size - 1){
           minNode = pre -> next; 
           OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                    (void *) minNode, 
                    (OS_ERR *) &err);
           ll->tail = pre;
           pre->next = NULL;
        }
        else{
           minNode = pre->next;
           OSMemPut((OS_MEM *) SchedListPartitionPtr, 
                      (void *) minNode, 
                      (OS_ERR *) &err);
           cur = pre->next->next;
           pre->next = cur;
        }
        ll->size--;
      }
      break;
    }
    pre = cur;
    cur = cur -> next;
  }
}