#include <os.h>
#include <OS_Task_Scheduling.h>
#include <stddef.h>
#include <os_cfg_app.h>
#include <stdlib.h>

#define MEM_QTY 60
#define OS_CFG_REC_RLS_TASK_PRIO 11u                                                    
#define OS_CFG_REC_RLS_TASK_STK_SIZE 128u   

#define OS_CFG_REC_RLS_TASK_STK_LIMIT ((OS_CFG_REC_RLS_TASK_STK_SIZE  * OS_CFG_TASK_STK_LIMIT_PCT_EMPTY) / 100u)

/*
************************************************************************************************************************
************************************************************************************************************************
*                                                  D A T A   T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/
typedef struct rb_list_node{
        int period;
        int key;
	OS_TCB *value;
        struct rb_list_node* next;
}rb_list_node;

typedef struct _rb_linkedlist{ 
        rb_list_node* head; 
        rb_list_node* tail;
        int size;
}rb_linkedlist;

typedef struct rb_node{
	int key;
	rb_linkedlist ll;
        int color;
	struct rb_node *left;
	struct rb_node *right;
}rb_node;

typedef struct rb_tree{
	rb_node *root;
	int min;
}OS_RBTree;

/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
extern OS_TCB OSRecRlsTaskTCB;
extern OS_RBTree OSRecTaskTree;
extern OS_MEM *RecPartitionPtr;
extern OS_MEM *RecListPartitionPtr;
extern CPU_STK OSCfg_RecRlsTaskStk[OS_CFG_REC_RLS_TASK_STK_SIZE];
extern CPU_STK * const  OSCfg_RecRlsTaskStkBasePtr;
extern CPU_STK_SIZE const  OSCfg_RecRlsTaskStkLimit;
extern CPU_STK_SIZE const  OSCfg_RecRlsTaskStkSize;


/*
************************************************************************************************************************
************************************************************************************************************************
*                   				  FUNTION PROTOTYPES
************************************************************************************************************************
************************************************************************************************************************
*/
/* ================================================================================================================== */
/*                                                 TASK MANAGEMENT                                                    */
/* ================================================================================================================== */
void OSRecTaskCreate(OS_TCB        *p_tcb,
                    CPU_CHAR      *p_name,
                    int			   period,
                    OS_TASK_PTR    p_task,
                    void          *p_arg,
                    OS_PRIO        prio,
                    CPU_STK       *p_stk_base,
                    CPU_STK_SIZE   stk_limit,
                    CPU_STK_SIZE   stk_size,
                    OS_MSG_QTY     q_size,
                    OS_TICK        time_quanta,
                    void          *p_ext,
                    OS_OPT         opt,
                    OS_ERR        *p_err);
void  OSTaskRecDel (OS_TCB  *p_tcb,
                 OS_ERR  *p_err);
void OSRecStart();
void pushTasksToReady(rb_node* root);
void pushTaskToReady(rb_node* node);
void OS_RecTreeUpdate();
void OS_RecRlsTask();
void  OS_RecTaskInit(OS_ERR  *p_err);
/* ================================================================================================================== */
/*                                                 RECURSIVE TREE MANAGEMENT                                                    */
/* ================================================================================================================== */
void OSRecTaskTreeInsert(int period, OS_TCB *value);
void OSRecTaskTreeRemoveMin();
/* ================================================================================================================== */
/*                                                 RED BLACK TREE FUNCTIONS                                                   */
/* ================================================================================================================== */
void createRecPartition();
void rb_tree_insert(OS_RBTree* tree, int key, OS_TCB *value);
rb_node* rb_tree_remove_min(OS_RBTree* tree);
rb_node* rb_node_right_rotate(rb_node* root);
rb_node* rb_node_left_rotate(rb_node* root);
rb_node* rb_node_double_right_rotate(rb_node* root);
rb_node* rb_node_double_left_rotate(rb_node* root);
int is_red(rb_node* root);
rb_node* create_rb_node(int key, OS_TCB* value);
rb_node* recreate_rb_node(rb_list_node* node);
rb_node* rb_tree_insert_node(rb_node* root, int key, OS_TCB *value);
rb_node* rb_tree_reinsert_node(rb_node* root, rb_list_node* newNode);
rb_node* move_red_left(rb_node* root);
rb_node* fix(rb_node* root);
void flip_color(rb_node* root);
rb_node* rb_tree_remove_min_node(rb_node* root, int* min, rb_node** minNode);
rb_node* rb_tree_reinsert_node(rb_node* root, rb_list_node* newNode);
void rb_tree_reinsert(OS_RBTree* tree, rb_list_node* newNode);
void printTreee(rb_node* root);

void createRecListPartition();
void release_rb_node(rb_node* node);
void printList(rb_linkedlist* ptr_ll);
rb_list_node * findNode(rb_linkedlist *ll, int index);
rb_list_node * insertNode(rb_linkedlist *ll, int index, int period, OS_TCB* value);
rb_list_node * reinsertNode(rb_linkedlist *ll, int index, rb_list_node* newNode);
rb_list_node * removeNode(rb_linkedlist *ll, int index);
void treeMinSearch(rb_node* root, int* min);
void rb_remove(OS_RBTree* tree, int key, int* min, rb_node** minNode);