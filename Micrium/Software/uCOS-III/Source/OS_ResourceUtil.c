#include <OS_ResourceUtil.h>
#include <OS_Task_Scheduling.h>

OS_MEM *ResourcePartitionPtr;
OS_MEM *StackResourcePartitionPtr;
OS_MEM *ResourceListPartitionPtr;
avl_tree OSResourceTree;
OS_ResourceStack OSResourceStack;

void createResourcePartition(){
    int block_size = sizeof(avl_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    ResourcePartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (ResourcePartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) ResourcePartitionPtr, 
        (CPU_CHAR *) "Resource Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

void createResourceListPartition(){
    int block_size = sizeof(avl_list_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    ResourceListPartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (ResourceListPartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) ResourceListPartitionPtr, 
        (CPU_CHAR *) "Resource List Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

void createStackResourcePartition(){
    int block_size = sizeof(stack_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    StackResourcePartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (StackResourcePartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) StackResourcePartitionPtr, 
        (CPU_CHAR *) "Stack Resource Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

/*******************************************************/
/*                   AVL TREE FUNCTIONS                */
/*******************************************************/
void release_avl_node(avl_node* node){
        OS_ERR err;  
	OSMemPut((OS_MEM *) ResourcePartitionPtr, 
                  (void *) node, 
                  (OS_ERR *) &err);
}

avl_node* avl_node_right_rotate(avl_node *root){
    avl_node* temp = root -> left;
    int rlh, rrh, slh;

    /* Rotate */
    root -> left = temp -> right;
    temp -> right = root;

    /* Update balance factors */
    rlh = height(root -> left);
    rrh = height(root -> right);
    slh = height(temp -> left);

    root -> height = max(rlh, rrh) + 1;
    temp -> height = max(slh, root -> height) + 1;

    return temp;
}

avl_node* avl_node_left_rotate(avl_node *root){
    avl_node* temp = root -> right;
    int rlh, rrh, slh;

    /* Rotate */
    root -> right = temp -> left;
    temp -> left = root;

    /* Update balance factors */
    rlh = height(root -> left);
    rrh = height(root -> right);
    slh = height(temp -> right);

    root -> height = max(rlh, rrh) + 1;
    temp -> height = max(slh, root -> height) + 1;

    return temp;
}

avl_node* avl_node_double_right_rotate(avl_node* root){
    root -> left = avl_node_left_rotate(root -> left);
    return avl_node_right_rotate(root);
}

avl_node* avl_node_double_left_rotate(avl_node* root){
    root -> right = avl_node_right_rotate(root -> right);
    return avl_node_left_rotate(root);
}

avl_node* create_avl_node(int key, OS_TCB* value){
        OS_ERR err;  
        avl_node* newNode = (avl_node *)OSMemGet((OS_MEM *)ResourcePartitionPtr, 
                            (OS_ERR *)&err);

	if(newNode != NULL){
		newNode -> key = key;
                newNode -> ll.size = 0;
                insertNode(&(newNode -> ll), 0, value);
		newNode -> height = 0;
		newNode -> right = NULL;
		newNode -> left = NULL;
	}
        return newNode;
}

avl_node* avl_tree_insert_node(avl_node* root, int key, OS_TCB* value, int* done){
    if (root == NULL){
        root = create_avl_node(key, value);
    }
    else{
//        int dir = root->data < data;
        int lh, rh, max;

//        root->link[dir] = jsw_insert_r(root->link[dir], data, done);
        if(key == root->key){
          insertResourceNode(&(root -> ll), 0, value);
        }
        else if(key > root -> key ){
          //go to the right
          root -> right = avl_tree_insert_node(root -> right, key, value, done);
          
          if (!*done){
            /* Rebalance if necessary */
            lh = height(root -> right);
            rh = height(root -> left);

            if (lh - rh >= 2){
                avl_node* a = root -> right -> right;
                avl_node* b = root -> right -> left;

                if (height(a) >= height(b)){
                    root = avl_node_left_rotate(root);
                }
                else{
                    root = avl_node_double_left_rotate(root);
                }
                *done = 1;
            }

            /* Update balance factors */
            lh = height(root -> right);
            rh = height(root -> left);
            max = max(lh, rh);

            root->height = max + 1;
          }
        }
        else{
          //go to the left
          root -> left = avl_tree_insert_node(root -> left, key, value, done);
          if (!*done){
            /* Rebalance if necessary */
            lh = height(root -> left);
            rh = height(root -> right);

            if (lh - rh >= 2){
                avl_node* a = root-> left-> left;
                avl_node* b = root-> left-> right;

                if (height(a) >= height(b)){
                    root = avl_node_right_rotate(root);
                }
                else{
                    root = avl_node_double_right_rotate(root);
                }

                *done = 1;
            }

            /* Update balance factors */
            lh = height(root -> left);
            rh = height(root -> right);
            max = max(lh, rh);

            root -> height = max + 1;
          }
        }
    }

    return root;
}

void avl_tree_insert(avl_tree* tree, int key, OS_TCB* value){
    int done = 0;
//    printf("key = %d\n", key);
    tree->root = avl_tree_insert_node(tree->root, key, value, &done);
    if(tree -> min > key || tree-> min == 0)
      tree -> min = key;
}

avl_node* avl_tree_remove_min(avl_tree* tree){
    int done = 0, min;
    avl_node* minNode;
    min = tree -> min;
    avl_node* curNode;
    int flag = 0;
//    printf("min = %d\n", min);
    if(tree -> root -> key == min){
//      printf("remove root\n");
      tree -> min = (tree -> root -> left != NULL) ? tree -> root -> right-> key : 
      (tree -> root -> right != NULL) ? tree -> root -> left-> key : 0;
//      return tree -> root;
      flag = 1;
    }
//      printf("min new = %d\n", tree -> min);
    tree -> root = avl_tree_remove_node(tree->root, min, &(tree -> min), &done, &minNode);
    
//      printf("min new = %d\n", tree -> min);
    curNode = tree -> root;
    if(flag != 1){
      while(curNode -> left != NULL){
        curNode = curNode -> left;
      }
      tree -> min = curNode -> key;
    }
    return minNode;
}

avl_node* avl_tree_remove_node(avl_node* root, int key, int* min, int *done, avl_node** minNode){
  
    if (root != NULL){
        int dir;

        /* Remove node */
        if (root -> key == key){
            *minNode = root;
            /* Unlink and fix parent */
            if (root -> left == NULL || root -> right == NULL){
                avl_node* temp;
                
//                dir = root->link[0] == NULL;
//                save = root->link[dir];
//                free(root);
                if(root -> left == NULL){
                  temp = root -> right;
                }
                else{
                  temp = root -> left;
                }
                return temp;
            }
            else{
                /* Find inorder predecessor */
                avl_node* heir = root -> left;

                while (heir -> right != NULL){
                    heir = heir -> right;
                }

                /* Copy and set new search data */
                root -> key = heir -> key;
                root -> ll = heir -> ll;
                key = heir -> key;
            }
        }

//        dir = root->data < data;
//        root->link[dir] = jsw_remove_r(root->link[dir], data, done);
        if(key > root -> key ){
          //we go to the right
          root -> right = avl_tree_remove_node(root -> right, key, min, done, minNode);
          if (!*done){
            /* Update balance factors */
            int lh = height(root -> right);
            int rh = height(root -> left);
            int max = max(lh, rh);

            root -> height = max + 1;

            /* Terminate or rebalance as necessary */
            if (lh - rh == -1){
                *done = 1;
            }

            if (lh - rh <= -2){
                avl_node* a = root -> left -> right;
                avl_node* b = root -> left -> left;

                if (height(a) <= height(b)){
                    root = avl_node_right_rotate(root);
                }
                else
                {
                    root = avl_node_double_right_rotate(root);
                }
            }
          }
        }
        else{
          //we go to the left
//          if(root -> left != NULL && root -> left -> key == key){
//            *min = root -> key;
//          }
          root -> left = avl_tree_remove_node(root -> left, key, min, done, minNode);
          if (!*done){
            /* Update balance factors */
            int lh = height(root -> left);
            int rh = height(root -> right);
            int max = max(lh, rh);

            root -> height = max + 1;

            /* Terminate or rebalance as necessary */
            if (lh - rh == -1){
                *done = 1;
            }

            if (lh - rh <= -2){
                avl_node* a = root -> right -> left;
                avl_node* b = root -> right -> right;

                if (height(a) <= height(b)){
                    root = avl_node_left_rotate(root);
                }
                else{
                    root = avl_node_double_left_rotate(root);
                }
            }
          }
        }
    }

    return root;
}

avl_list_node* insertResourceNode(avl_linkedlist *ll, int index, OS_TCB* value){
   OS_ERR err;  
   avl_list_node *pre, *cur;
   
   if (ll == NULL || index < 0 || index > ll->size + 1)
      return NULL;
   
   // If empty list or inserting first node, need to update head pointer
   if (ll->head == NULL || index == 0){
      cur = ll->head;
      ll->head = (avl_list_node *)OSMemGet((OS_MEM *)ResourceListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = ll->head;
//      ll->head->key = period;
//      ll->head->period = period;
      ll->head->value = value;
      ll->head->next = cur;
      ll->size++;
      return ll->head;
   }
   
   // Inserting as new last node
   if (index == ll->size){
      pre = ll->tail;
      cur = pre->next;
      pre->next = (avl_list_node *)OSMemGet((OS_MEM *)ResourceListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = pre->next;
      
//      pre->next->key = period;
//      pre->next->period = period;
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   // Find the nodes before and at the target position
   // Create a new node and reconnect the links
   if ((pre = findResourceNode(ll, index-1)) != NULL){
      cur = pre->next;
      pre->next = (avl_list_node *)OSMemGet((OS_MEM *)ResourceListPartitionPtr, 
                        (OS_ERR *)&err);
      
      if (index == ll->size)
         ll->tail = pre->next;
      
//      pre->next->key = period;
//      pre->next->period = period;
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   return NULL;
}

avl_list_node* findResourceNode(avl_linkedlist *ll, int index){
   
   avl_list_node *temp;
   
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   temp = ll->head;
   
   if (temp == NULL || index < 0)
      return NULL;
   
   while (index > 0){
      temp = temp->next;
      if (temp == NULL)
         return NULL;
      index--;
   }
   
   return temp;
}
              
avl_list_node* removeResourceListNode(avl_linkedlist *ll, int index){
   OS_ERR err;  
   avl_list_node *pre, *cur, *minNode = NULL;
   
   // Highest index we can remove is size-1
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   // If removing first node, need to update head pointer
   if (index == 0){
      minNode = ll->head;
      OSMemPut((OS_MEM *) ResourceListPartitionPtr, 
                (void *) minNode, 
                (OS_ERR *) &err);
      cur = ll->head->next;
      ll->head = cur;
      ll->size--;
      
      if (ll->size == 0)
         ll->tail = 0;
      
      return minNode;
   }
   
   // Find the nodes before and after the target position
   // Free the target node and reconnect the links
   if ((pre = findResourceNode(ll, index-1)) != NULL){
      
      // Removing the last node, update the tail pointer
      if (index == ll->size - 1){
         minNode = pre -> next; 
         OSMemPut((OS_MEM *) ResourceListPartitionPtr, 
                  (void *) minNode, 
                  (OS_ERR *) &err);
         ll->tail = pre;
         pre->next = NULL;
      }
      else{
         minNode = pre->next;
         OSMemPut((OS_MEM *) ResourceListPartitionPtr, 
                    (void *) minNode, 
                    (OS_ERR *) &err);
         cur = pre->next->next;
         pre->next = cur;
      }
      ll->size--;
      return minNode;
   }
   
   return NULL;
}
            
/*******************************************************/
/*                   STACK FUNCTIONS                   */
/*******************************************************/

int isEmptyStack(OS_ResourceStack* s){
  if (s -> size == 0) return 1;
  return 0;
}

int peek(OS_ResourceStack* s){
  if (s -> top !=NULL)  
    return s -> top -> ceiling;
  else 
    return 0;
}

void push(OS_ResourceStack* s, OS_MUTEX* item, OS_TCB *blocking_task, int owner_original_priority, int ceiling){ 
  OS_ERR err;  
  s -> top = (stack_node *)OSMemGet((OS_MEM *)StackResourcePartitionPtr, 
             (OS_ERR *)&err);
  s -> top -> mutex = item;
//  printf("PUSH CEILING 1= %d\n", ceiling);
  s -> top -> blocking_task = blocking_task;
  s -> top -> owner_original_priority = owner_original_priority;
//  printf("owner1 = %d\n", owner_original_priority);
//  printf("owner2 = %d\n", s -> top -> owner_original_priority);
  s -> top -> ceiling = ceiling;
//  printf("PUSH CEILING = %d\n", s -> top -> ceiling);
  (s->size)++;
}

OS_MUTEX* pop(OS_ResourceStack *s){
  OS_ERR err;  
  OS_MUTEX* item;
  if (!isEmptyStack(s)) {
    item = s -> top -> mutex;
//    printf("POP CEILING 1= %d\n", s -> top -> ceiling);
    OSMemPut((OS_MEM *) StackResourcePartitionPtr, 
            (void *) s -> top, 
            (OS_ERR *) &err);
    (s -> size)--;
    (s -> top)--;
//    printf("POP CEILING 2= %d\n", s -> top -> ceiling);
    return item;
  }
  else 
    return NULL;
}

OS_TCB* peekTCB(OS_ResourceStack* s){
  if (s -> top !=NULL)  
    return s -> top -> blocking_task;
  else 
    return NULL;
}

int peekPrio(OS_ResourceStack* s){
  if (s -> top !=NULL)  
    return s -> top -> owner_original_priority;
  else 
    return 0;
}









