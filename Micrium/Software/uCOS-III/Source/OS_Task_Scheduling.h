#define MEM_QTY 60

#include <os.h>
#include <stddef.h>
#include <os_cfg_app.h>
#include <stdlib.h>

#define PARENT(x) (x - 1) / 2
/*
************************************************************************************************************************
************************************************************************************************************************
*                                                  D A T A   T Y P E S
************************************************************************************************************************
************************************************************************************************************************
*/
typedef struct _heap_list_node{
	OS_TCB *value;
        struct _heap_list_node* next;
}heap_list_node;

typedef struct _heap_linkedlist{ 
        heap_list_node* head; 
        heap_list_node* tail;
        int size;
}heap_linkedlist;

typedef struct _heapNode{
	int key;
	heap_linkedlist ll;
}heapNode;

typedef struct _OS_MinHeap{
	int size;
	heapNode *items;
}OS_MinHeap;

/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
extern OS_MinHeap OSRdyMinHeap;
extern OS_MEM *SchedPartitionPtr, *SchedListPartitionPtr;
extern int tickCtr, secondCtr;
extern int schedEnable;
//extern int flagl;

/*
************************************************************************************************************************
************************************************************************************************************************
*                   				  FUNTION PROTOTYPES
************************************************************************************************************************
************************************************************************************************************************
*/
/* ================================================================================================================== */
/*                                                 HEAP MANAGEMENT                                                    */
/* ================================================================================================================== */
void createSchedPartition();
void createSchedListPartition();        
void heap_insert(OS_MinHeap* heap, int key, OS_TCB* item);
heapNode* createNode(int key);//, OS_TCB* item);
void swap(heapNode* node1, heapNode* node2);
void heapify(OS_MinHeap* heap, int i);
heapNode* deleteNode(OS_MinHeap *heap);
OS_TCB* getMinNode(OS_MinHeap *heap);
heap_list_node* insertHeapListNode(heap_linkedlist *ll, int index, OS_TCB* value);
heap_list_node* removeHeapListNode(heap_linkedlist *ll, int index);
heap_list_node* findHeapListNode(heap_linkedlist *ll, int index);
void removeHeapListByValue(heap_linkedlist *ll, OS_TCB* p_tcb);
