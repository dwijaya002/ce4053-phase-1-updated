#include <OS_RecTask.h>

void createRecPartition(){
    int block_size = sizeof(rb_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    RecPartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (RecPartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) RecPartitionPtr, 
        (CPU_CHAR *) "Rec Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

void createRecListPartition(){
    int block_size = sizeof(rb_list_node);
    void *p_storage;// = malloc(MEM_QTY * block_size); 
    OS_ERR err;
    RecListPartitionPtr = (OS_MEM *)malloc(sizeof(OS_MEM));
    if (RecListPartitionPtr != (OS_MEM *)0) {
      p_storage = (void *)malloc(MEM_QTY * block_size); 
      if (p_storage != (void *)0) {
        OSMemCreate((OS_MEM *) RecListPartitionPtr, 
        (CPU_CHAR *) "Rec List Partition",
        (void *) p_storage, 
        (OS_MEM_QTY ) MEM_QTY, 
        (OS_MEM_SIZE) block_size,
        (OS_ERR *) &err);
      }
    }
}

rb_node* rb_node_right_rotate(rb_node* root){
	//temp will be the new root node returned
	rb_node* temp = root -> left;
	
	root -> left = temp -> right;
	temp -> right = root;

	//modify the color
	root -> color = 1;
	temp -> color = 0;

	return temp;
}

rb_node* rb_node_left_rotate(rb_node* root){
	//temp will be the new root node returned
	rb_node* temp = root -> right;
	
	root -> right = temp -> left;
	temp -> left = root;

	//modify the color
	root -> color = 1;
	temp -> color = 0;

	return temp;
}

rb_node* rb_node_double_right_rotate(rb_node* root){
	root -> left = rb_node_left_rotate(root -> left);
	return rb_node_right_rotate(root);
}

rb_node* rb_node_double_left_rotate(rb_node* root){
	root -> right = rb_node_right_rotate(root -> right);
	return rb_node_left_rotate(root);
}

int is_red(rb_node* root){
    int a = 0; 
    if((root != NULL) && ((root -> color) == 1))
      a = 1;
    return a;
}

void release_rb_node(rb_node* node){
        OS_ERR err;  
	OSMemPut((OS_MEM *) RecPartitionPtr, 
                  (void *) node, 
                  (OS_ERR *) &err);
}

rb_node* create_rb_node(int key, OS_TCB* value){
        OS_ERR err;  
	//rb_node* newNode = malloc(sizeof(rb_node));
        rb_node* newNode = (rb_node *)OSMemGet((OS_MEM *)RecPartitionPtr, 
                            (OS_ERR *)&err);
//        if (err == OS_ERR_NONE) {
//            //printf("NO ERROR\n");
//        }
//        else 
//          //printf("ERROR\n");
	if(newNode != NULL){
//                //printf("NOT NULL\n");
		newNode -> key = key;
//		newNode -> period = key;
//                newNode -> value = 
                newNode->ll.size = 0;
                insertNode(&(newNode -> ll), 0, key, value);
		newNode -> color = 1;
		newNode -> right = NULL;
		newNode -> left = NULL;
	}
//        else 
//          //printf("NULL\n");
        return newNode;
}

rb_node* recreate_rb_node(rb_list_node* node){
        OS_ERR err;  
        int key = node -> key;
        OS_TCB* value = node -> value;
	//rb_node* newNode = malloc(sizeof(rb_node));
        rb_node* newNode = (rb_node *)OSMemGet((OS_MEM *)RecPartitionPtr, 
                            (OS_ERR *)&err);
	if(newNode != NULL){
                //////printf("NOT NULL\n");
		newNode -> key = key;
//		newNode -> period = node->period;
//                newNode -> value = 
                newNode->ll.size = 0;
                reinsertNode(&(newNode -> ll), 0, node);
		newNode -> color = 1;
		newNode -> right = NULL;
		newNode -> left = NULL;
	}
//        else 
//          //printf("NULL\n");
        return newNode;
}

rb_node* rb_tree_insert_node(rb_node* root, int key, OS_TCB *value){
	if(root == NULL){
                //printf("root period = %d\n", key);
		root = create_rb_node(key, value);
	}
	else{
                if(key == root->key){
                  insertNode(&(root -> ll), 0, key, value);
                }
		else if(key < root->key){
			//go to the left
			root -> left = rb_tree_insert_node(root -> left, key, value);
			//balance the tree
			if(is_red(root -> left) == 1){
                //case 1
	            if (is_red(root -> right) == 1){
	                root -> color = 1;
	                root-> left -> color = 0;
	                root-> right -> color = 0;
	            }
	            else{
	                //case 2
	                if (is_red(root-> left -> left) == 1){
	                    root = rb_node_right_rotate(root);
	                }
	                //case 3
	                else if (is_red(root-> left -> right) == 1){
	                    root = rb_node_double_right_rotate(root);
	                }
            	}
            }
        }
		else{
			//go to the right
			root -> right = rb_tree_insert_node(root -> right, key, value);
			//balance the tree
			if(is_red(root -> right)){
                //case 1
	            if (is_red(root -> left)){
	                root -> color = 1;
	                root-> right -> color = 0;
	                root-> left -> color = 0;
	            }
	            else{
	                //case 2
	                if (is_red(root-> right -> right)){
	                    root = rb_node_left_rotate(root);
	                }
	                //case 3
	                else if (is_red(root-> right -> left)){
	                    root = rb_node_double_left_rotate(root);
	                }
            	}
            }
		}
	}
	return root;
}

rb_node* rb_tree_reinsert_node(rb_node* root, rb_list_node* newNode){
	if(root == NULL){
//                printf("recreate\n");
		root = recreate_rb_node(newNode);
	}
	else{
                if(newNode -> key == root->key){
                  reinsertNode(&(root -> ll), 0, newNode);
                }
		else if(newNode -> key < root->key){
			//go to the left
			root -> left = rb_tree_reinsert_node(root -> left, newNode);
			//balance the tree
			if(is_red(root -> left) == 1){
                //case 1
	            if (is_red(root -> right) == 1){
	                root -> color = 1;
	                root-> left -> color = 0;
	                root-> right -> color = 0;
	            }
	            else{
	                //case 2
	                if (is_red(root-> left -> left) == 1){
	                    root = rb_node_right_rotate(root);
	                }
	                //case 3
	                else if (is_red(root-> left -> right) == 1){
	                    root = rb_node_double_right_rotate(root);
	                }
            	}
            }
        }
		else{
			//go to the right
			root -> right = rb_tree_reinsert_node(root -> right, newNode);
			//balance the tree
			if(is_red(root -> right) == 1){
                //case 1
	            if (is_red(root -> left) == 1){
	                root -> color = 1;
	                root-> right -> color = 0;
	                root-> left -> color = 0;
	            }
	            else{
	                //case 2
	                if (is_red(root-> right -> right) == 1){
	                    root = rb_node_left_rotate(root);
	                }
	                //case 3
	                else if (is_red(root-> right -> left) == 1){
	                    root = rb_node_double_left_rotate(root);
	                }
            	}
            }
		}
	}
	return root;
}

void rb_tree_insert(OS_RBTree* tree, int key, OS_TCB *value){
	tree -> root = rb_tree_insert_node(tree -> root, key, value);
        //////printf("root period 2 = %d\n", tree -> root -> key);
	//the color of the root node is always black
	tree -> root -> color = 0;
        //update the minimum value of the tree
        if(tree -> min > key || tree-> min == 0)
          tree -> min = key;
}

void rb_tree_reinsert(OS_RBTree* tree, rb_list_node* newNode){
        tree -> root = rb_tree_reinsert_node(tree -> root, newNode);
        //the color of the root node is always black
	tree -> root -> color = 0;
        //update the minimum value of the tree
        if(tree->min > newNode->key)
          tree -> min = newNode -> key;
}       

void flip_color(rb_node* root){
//	root -> color = ~(root ->color);
        root -> color = (root->color == 1) ? 0 : 1;
        if(root -> left != NULL)
          root -> left -> color = (root -> left -> color == 1) ? 0 : 1;
        if(root -> right != NULL)
          root -> right -> color = (root -> right -> color == 1) ? 0 : 1;
}

rb_node* move_red_left(rb_node* root){
	flip_color(root);
	if(is_red(root -> right -> left) == 1){
		root -> right = rb_node_right_rotate(root -> right);
		root = rb_node_left_rotate(root);
		flip_color(root);
	}
	return root;
}

rb_node* fix(rb_node* root){
//        printf("root = %d\n", root -> key);
//        printf("root right = %d\n", root -> right -> color);
//        printf("root right red = %d\n", is_red(root -> right));
	if(is_red(root -> right) == 1){
//                printf("a\n");
		root = rb_node_left_rotate(root);
	}
	if((is_red(root -> left) == 1) && (is_red(root -> left -> left) == 1)){
//                printf("b\n");
		root = rb_node_right_rotate(root);
	}
	if((is_red(root -> left) == 1) && (is_red(root -> right) == 1)){
//                printf("c\n");
		flip_color(root);
	}
	return root;
}
void printTreeec(rb_node* root){
  if(root == NULL){
    return;
  }
//  printf("color = %d\n", root->color);
  printTreeec(root -> left);       
  printTreeec(root -> right);
}
rb_node* rb_tree_remove_min_node(rb_node* root, int* min, rb_node** minNode){
	if(root -> left == NULL){
//                printf("REMOVE NULL\n");
                *minNode = root;
                return NULL;
	}
//        printf("REMOVE NOT NULL\n");
	if(root -> left -> left == NULL){
		//update the current minimum value
		*min = root -> key;
	}
//        if(root -> left != NULL && root -> left -> left != NULL){
          if(is_red(root -> left) == 0 && is_red(root -> left -> left) == 0){
                  root = move_red_left(root);
          }
//        }
	root -> left = rb_tree_remove_min_node(root -> left, min, minNode);

	return fix(root);
}

rb_node* rb_tree_remove_min(OS_RBTree* tree){
	int min;
        rb_node* minTreeNode;
        if(tree -> root == NULL) return NULL;
	rb_remove(tree, tree -> min, &(tree -> min), &minTreeNode);
        tree -> root -> color = 0;

        return minTreeNode;
}

void OSRecTaskTreeInsert(int period, OS_TCB *value){
	rb_tree_insert(&OSRecTaskTree, period, value);
//        //////printf("period 1 = %d\n", period);
        //////printf("period = %d\n", OSRecTaskTree.root->key);
}

void OSRecTaskTreeRemoveMin(){
	rb_tree_remove_min(&OSRecTaskTree);
}

void printList(rb_linkedlist* ptr_ll){
    rb_list_node* cur = ptr_ll -> head;
    if (cur == NULL)  return; // or size ==0
      while (cur!= NULL){
        //printf("%s\n", cur -> value -> NamePtr); 
        cur = cur->next;
    }
    //printf("\n");
    return;
}

rb_list_node * findNode(rb_linkedlist *ll, int index){
   
   rb_list_node *temp;
   
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   temp = ll->head;
   
   if (temp == NULL || index < 0)
      return NULL;
   
   while (index > 0){
      temp = temp->next;
      if (temp == NULL)
         return NULL;
      index--;
   }
   
   return temp;
}
        
rb_list_node * insertNode(rb_linkedlist *ll, int index, int period, OS_TCB* value){
   OS_ERR err;  
   rb_list_node *pre, *cur;
   
   if (ll == NULL || index < 0 || index > ll->size + 1)
      return NULL;
   
   // If empty list or inserting first node, need to update head pointer
   if (ll->head == NULL || index == 0){
      cur = ll->head;
      ll->head = (rb_list_node *)OSMemGet((OS_MEM *)RecListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = ll->head;
      ll->head->key = period;
      ll->head->period = period;
      ll->head->value = value;
      ll->head->next = cur;
      ll->size++;
      return ll->head;
   }
   
   // Inserting as new last node
   if (index == ll->size){
      pre = ll->tail;
      cur = pre->next;
      pre->next = (rb_list_node *)OSMemGet((OS_MEM *)RecListPartitionPtr, 
                            (OS_ERR *)&err);
      ll->tail = pre->next;
      
      pre->next->key = period;
      pre->next->period = period;
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   // Find the nodes before and at the target position
   // Create a new node and reconnect the links
   if ((pre = findNode(ll, index-1)) != NULL){
      cur = pre->next;
      pre->next = (rb_list_node *)OSMemGet((OS_MEM *)RecListPartitionPtr, 
                        (OS_ERR *)&err);
      
      if (index == ll->size)
         ll->tail = pre->next;
      
      pre->next->key = period;
      pre->next->period = period;
      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   return NULL;
}

rb_list_node * reinsertNode(rb_linkedlist *ll, int index, rb_list_node* newNode){
   OS_ERR err;  
   rb_list_node *pre, *cur;
   
   if (ll == NULL || index < 0 || index > ll->size + 1)
      return NULL;
   
   // If empty list or inserting first node, need to update head pointer
   if (ll->head == NULL || index == 0){
      cur = ll->head;
      ll->head = newNode;
      ll->tail = ll->head;
//      ll->head->key = key;
//      ll->head->period = period;
//      ll->head->value = value;
      ll->head->next = cur;
      ll->size++;
      return ll->head;
   }
   
   // Inserting as new last node
   if (index == ll->size){
      pre = ll->tail;
      cur = pre->next;
      pre->next = newNode;
      ll->tail = pre->next;
      
//      pre->next->key = key;
//      pre->next->period = period;
//      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   // Find the nodes before and at the target position
   // Create a new node and reconnect the links
   if ((pre = findNode(ll, index-1)) != NULL){
      cur = pre->next;
      pre->next = newNode;
      
      if (index == ll->size)
         ll->tail = pre->next;
      
//      pre->next->key = key;
//      pre->next->period = period;
//      pre->next->value = value;
      pre->next->next = cur;
      ll->size++;
      return pre->next;
   }
   
   return NULL;
}

rb_list_node * removeNode(rb_linkedlist *ll, int index){
   OS_ERR err;  
   rb_list_node *pre, *cur, *minNode = NULL;
   
   // Highest index we can remove is size-1
   if (ll == NULL || index < 0 || index >= ll->size)
      return NULL;
   
   // If removing first node, need to update head pointer
   if (index == 0){
      minNode = ll->head;
      cur = ll->head->next;
      ll->head = cur;
      ll->size--;
      
      if (ll->size == 0)
         ll->tail = 0;
      
      return minNode;
   }
   
   // Find the nodes before and after the target position
   // Free the target node and reconnect the links
   if ((pre = findNode(ll, index-1)) != NULL){
      
      // Removing the last node, update the tail pointer
      if (index == ll->size - 1){
         minNode = pre -> next; 
         ll->tail = pre;
         pre->next = NULL;
      }
      else{
         minNode = pre->next;
         cur = pre->next->next;
         pre->next = cur;
      }
      ll->size--;
      return minNode;
   }
   
   return NULL;
}

void treeMinSearch(rb_node* root, int* min){
 // printf("min search start %d\n", *min);
  //printf("root key %d\n", root->key);
  if(root->left != NULL)
  treeMinSearch(root->left,min);
  if(*min == 0 || *min > root->key) *min = root->key;
  //if(root->right != NULL)
  //treeMinSearch(root->right, min);
  //printf("Min Temp value is %d", *min);
  return;
}
void rb_remove(OS_RBTree* tree, int key, int* min, rb_node** minNode){
    if (tree->root != NULL){
        rb_node head = { 0 }; /* False tree root */
        rb_node *q, *p, *g; /* Helpers */
        rb_node *f = NULL;  /* Found item */
        int  dir2, st, sp, sr, sk, mp, mr, kp, kl;
        int dir = 1;
        rb_node *s;
        int last;
        /* Set up helpers */
        q = &head;
        g = p = NULL;
//        q->link[1] = tree->root;
        q -> right = tree -> root;
        /* Search and push a red down */
        if(tree -> root -> key == key) 
          *min = (tree -> root -> left == NULL) ? tree -> root -> right-> key : 
          (tree -> root -> right == NULL) ? tree -> root -> left-> key : 0;
        while (1){
            if(dir == 1){
              if(q -> right == NULL) break;
            }
            else 
              if(q -> left == NULL) break;
            
            last = dir;

            /* Update helpers */
            g = p, p = q;
            if(dir == 1){
              q = q->right;
            }
            else{
              q = q->left;
            }
            dir = (q -> key < key);
            
            /* Save found node */
            if(q->left != NULL && q->left->key == key) *min = q -> key;
            if(q->right != NULL && q->right->key == key) *min = q -> key;
            if (q -> key == key){
                f = q;
            }
            
            st = (is_red(q) == 1) ? 0:1;
            if(dir == 1){
              sp = (is_red(q->right)== 1) ?  0 :1;
              sr = (is_red(q->left) == 1) ? 1:0;
              sk = (is_red(q->left) == 1) ? 0:1;
            }
            else{
              sp = (is_red(q->left) == 1) ?  0 :1;
              sr = (is_red(q->right)==1) ? 1 : 0;
              sk = (is_red(q->right) == 1) ? 0 : 1;
            }

            /* Push the red node down */
//            if (st && sp){
//                if (sr){
//                  if(dir == 1){
//                    if(last == 1){
//                      p = p -> right = rb_node_right_rotate(q);
//                    }
//                    else{
//                      p = p -> left = rb_node_right_rotate(q);
//                    }
//                  }
//                  else{
//                    if(last == 1){
//                      p = p -> right = rb_node_left_rotate(q);
//                    }
//                    else{
//                      p = p -> left = rb_node_left_rotate(q);
//                    }
//                  }
//                }
//                else if (sk)
//                {       
//                    if(last == 1) {
//                      s = p->left;
//                    }
//                    else{ 
//                      s = p->right;
//                    }
//                    if (s != NULL){
//                        if(last == 1){
//                          mp = (is_red(s->left) == 1) ? 0 : 1;
//                          mr = (is_red(s->right) == 1) ? 0 : 1;
//                        }
//                        else{
//                          mp = (is_red(s->right) == 1) ? 0 : 1;
//                          mr = (is_red(s->left) == 1) ? 0 : 1;
//                        }
//                        if (mp && mr){
//                            /* Color flip */
//                            p->color = 0;
//                            s->color = 1;
//                            q->color = 1;
//                        }
//                        else{
////                         
//                            dir2 = (g->right == p);
//                            if(last == 1){
//                              kp = is_red(s->right);
//                              kl = is_red(s->left);
//                            }
//                            else{
//                              kp = is_red(s->left);
//                              kl = is_red(s->right);
//                            }
//                            if (kp){
//                              if(dir2 == 1){
//                                if(last == 1){
//                                  g->right = rb_node_double_right_rotate(p);
//                                }
//                                else{
//                                  g->right = rb_node_double_left_rotate(p);
//                                }
//                              }
//                              else{
//                                if(last == 1){
//                                  g->left = rb_node_double_right_rotate(p);
//                                }
//                                else{
//                                  g->left = rb_node_double_left_rotate(p);
//                                }
//                              }
//                            }
//                            else if (kl){
//                              if(dir2 == 1){
//                                if(last == 1){
//                                  g->right = rb_node_right_rotate(p);
//                                }
//                                else{
//                                  g->right = rb_node_left_rotate(p);
//                                }
//                              }
//                              else{
//                                if(last == 1){
//                                  g->left = rb_node_right_rotate(p);
//                                }
//                                else{
//                                  g->left = rb_node_left_rotate(p);
//                                }
//                              }
//                            }
//
//                            /* Ensure correct coloring */
//                            if(dir2 == 1){
//                              q->color = g->right->color = 1;
//                              g->right->left->color = 0;
//                              g->right->right->color = 0;
//                            }
//                            else{
//                              q->color = g->left->color = 1;
//                              g->left->right->color = 0;
//                              g->left->right->color = 0;
//                            }
//                        }
//                    }
//                }
//            }
        }

        /* Replace and remove if found */
        if (f != NULL){
            f->key = q->key;
            if(p->right == q){
              if(q -> left == NULL){
                p->right = q->right;
              }
              else{
                p->right = q->left;
              }
            }
            else{
              if(q -> left == NULL){
                p->left = q->right;
              }
              else{
                p->left = q->left;
              }
            }
            *minNode = q;
        }

        /* Update root and make it black */
        tree->root = head.right;

        if (tree->root != NULL){
            tree->root->color = 0;
        }
    }
}