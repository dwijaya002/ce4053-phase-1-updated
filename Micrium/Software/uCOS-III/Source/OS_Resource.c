#include <OS_Resource.h>
#include <OS_RecTask.h>
#include <OS_ResourceUtil.h>

/*
************************************************************************************************************************
*                                                  GLOBAL VARIABLES
************************************************************************************************************************
*/
int OS_System_Ceiling = 0;

/*
************************************************************************************************************************
*                                                   CREATE A RESOURCE MUTEX
*
* Description: This function creates a mutex and set its resource ceiling.
*
* Arguments  : p_mutex       is a pointer to the mutex to initialize.  Your application is responsible for allocating
*                            storage for the mutex.
*
*              p_name        is a pointer to the name you would like to give the mutex.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE                    if the call was successful
*                                OS_ERR_CREATE_ISR              if you called this function from an ISR
*                                OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the Mutex after you called
*                                                                 OSSafetyCriticalStart().
*                                OS_ERR_NAME                    if 'p_name'  is a NULL pointer
*                                OS_ERR_OBJ_CREATED             if the mutex has already been created
*                                OS_ERR_OBJ_PTR_NULL            if 'p_mutex' is a NULL pointer
*
* Returns    : none
************************************************************************************************************************
*/
void OSResourceMutexCreate(OS_MUTEX *p_mutex, CPU_CHAR *p_name, OS_ERR *p_err, int ceiling){
  OSMutexCreate(p_mutex, p_name, p_err);
  p_mutex -> ceiling = ceiling;
}

/*
************************************************************************************************************************
*                                                   DELETE A RESOURCE MUTEX
*
* Description: This function deletes a resource mutex and readies all tasks pending on the mutex.
*
* Arguments  : p_mutex       is a pointer to the mutex to delete
*
*              opt           determines delete options as follows:
*
*                                OS_OPT_DEL_NO_PEND          Delete mutex ONLY if no task pending
*                                OS_OPT_DEL_ALWAYS           Deletes the mutex even if tasks are waiting.
*                                                            In this case, all the tasks pending will be readied.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE                 The call was successful and the mutex was deleted
*                                OS_ERR_DEL_ISR              If you attempted to delete the mutex from an ISR
*                                OS_ERR_OBJ_PTR_NULL         If 'p_mutex' is a NULL pointer.
*                                OS_ERR_OBJ_TYPE             If 'p_mutex' is not pointing to a mutex
*                                OS_ERR_OPT_INVALID          An invalid option was specified
*                                OS_ERR_STATE_INVALID        Task is in an invalid state
*                                OS_ERR_TASK_WAITING         One or more tasks were waiting on the mutex
*
* Returns    : == 0          if no tasks were waiting on the mutex, or upon error.
*              >  0          if one or more tasks waiting on the mutex are now readied and informed.
*
* Note(s)    : 1) This function must be used with care.  Tasks that would normally expect the presence of the mutex MUST
*                 check the return code of OSMutexPend().
*
*              2) OSMutexAccept() callers will not know that the intended mutex has been deleted.
*
*              3) Because ALL tasks pending on the mutex will be readied, you MUST be careful in applications where the
*                 mutex is used for mutual exclusion because the resource(s) will no longer be guarded by the mutex.
************************************************************************************************************************
*/
OS_OBJ_QTY OSResourceMutexDel(OS_MUTEX *p_mutex, OS_OPT opt, OS_ERR *p_err){
  return OSMutexDel(p_mutex, opt, p_err);
}

/*
************************************************************************************************************************
*                                                    PEND ON RESOURCE MUTEX
*
* Description: This function access a resource associated with the given resource mutex.
*
* Arguments  : p_mutex       is a pointer to the mutex
*
*              timeout       is an optional timeout period (in clock ticks).  If non-zero, your task will wait for the
*                            resource up to the amount of time (in 'ticks') specified by this argument.  If you specify
*                            0, however, your task will wait forever at the specified mutex or, until the resource
*                            becomes available.
*
*              opt           determines whether the user wants to block if the mutex is not available or not:
*
*                                OS_OPT_PEND_BLOCKING
*                                OS_OPT_PEND_NON_BLOCKING
*
*              p_ts          is a pointer to a variable that will receive the timestamp of when the mutex was posted or
*                            pend aborted or the mutex deleted.  If you pass a NULL pointer (i.e. (CPU_TS *)0) then you
*                            will not get the timestamp.  In other words, passing a NULL pointer is valid and indicates
*                            that you don't need the timestamp.
*
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*
*                                OS_ERR_NONE               The call was successful and your task owns the resource
*                                OS_ERR_MUTEX_OWNER        If calling task already owns the mutex
*
* Returns    : none
************************************************************************************************************************
*/
void OSResourceMutexPend(OS_MUTEX *p_mutex, OS_TICK timeout, OS_OPT opt, CPU_TS *p_ts, OS_ERR *p_err){
    
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();
    int blockingPrio;


#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    }
    /*if the priority of the task is lower than the current system ceiling and the task does not hold any resource*/
    if(OS_System_Ceiling != 0 && OSTCBCurPtr -> Prio >= OS_System_Ceiling && OSTCBCurPtr -> resourceCount == 0){
        CPU_CRITICAL_ENTER();
        /* Point to the TCB of the Mutex owner                    */
        p_tcb = peekTCB(&OSResourceStack);
        blockingPrio = peekPrio(&OSResourceStack);

        if (blockingPrio > OSTCBCurPtr->Prio) {
          /* See if mutex owner has a lower priority than current   */
          switch (p_tcb->TaskState) {
              case OS_TASK_STATE_RDY:                  
                    /* Remove from ready list at current priority             */
                   removeTCBHeap(&OSRdyMinHeap, p_tcb -> Prio, p_tcb); 
                   /* Raise owner's priority                                 */
                   p_tcb->Prio = OSTCBCurPtr->Prio;              
                   /* Insert in ready list at new priority                   */
                   heap_insert(&OSRdyMinHeap, p_tcb->Prio, p_tcb);
                   break;

              case OS_TASK_STATE_DLY:
              case OS_TASK_STATE_DLY_SUSPENDED:
              case OS_TASK_STATE_SUSPENDED:
                   p_tcb->Prio = OSTCBCurPtr->Prio;           /* Only need to raise the owner's priority                */
                   break;

              default:
                   *p_err = OS_ERR_STATE_INVALID;
                   break;
          }
        }

        /*put the task into blocked structure and remove the task from the ready structure*/
	OS_ResourcePend(OS_TASK_PEND_ON_MUTEX);
        CPU_CRITICAL_EXIT();
        OSSched();    
    }
    

    CPU_CRITICAL_ENTER();
    if (p_mutex->OwnerNestingCtr == (OS_NESTING_CTR)0) {    /* Resource available?                                    */
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          /* Yes, caller may proceed                                */
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
	(OSTCBCurPtr -> resourceCount)++;
        //update the system ceiling
        if(p_mutex -> ceiling < OS_System_Ceiling || OS_System_Ceiling == 0){
          OS_System_Ceiling = p_mutex -> ceiling;
        }
        //PUSH THE MUTEX INTO THE RESOURCE STACK
        push(&OSResourceStack, p_mutex, OSTCBCurPtr, OSTCBCurPtr->Prio, OS_System_Ceiling);
        
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err                     =  OS_ERR_NONE;
        return;
    }

    if (OSTCBCurPtr == p_mutex->OwnerTCBPtr) {              /* See if current task is already the owner of the mutex  */
        p_mutex->OwnerNestingCtr++;
        if (p_ts != (CPU_TS *)0) {
           *p_ts  = p_mutex->TS;
        }
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_OWNER;                        /* Indicate that current task already owns the mutex      */
        return;
    }

    CPU_CRITICAL_EXIT();
}
void  OS_ResourcePend (OS_STATE pending_on){
    OS_PEND_LIST  *p_pend_list;
    OS_ERR  err;

//    OSTCBCurPtr->PendOn     = pending_on;                    /* Resource not available, wait until it is              */
//    OSTCBCurPtr->PendStatus = OS_STATUS_PEND_OK;

//    change the state of the task to pending/blocked state
//    OSTCBCurPtr -> TaskState = OS_TASK_STATE_PEND;
    
    //remove the task from ready structure the currently running task
    deleteNode(&OSRdyMinHeap);
    
    //insert the task to the blocked structure
    avl_tree_insert(&OSResourceTree, OSTCBCurPtr -> Prio, OSTCBCurPtr);
}

/*
************************************************************************************************************************
*                                                   POST TO A RESOURCE MUTEX
*
* Description: This function releases the resouce associated with the given mutex
*
* Arguments  : p_mutex  is a pointer to the mutex
*
*              opt      is an option you can specify to alter the behavior of the post.  The choices are:
*
*                           OS_OPT_POST_NONE        No special option selected
*                           OS_OPT_POST_NO_SCHED    If you don't want the scheduler to be called after the post.
*
*              p_err    is a pointer to a variable that will contain an error code returned by this function.
*
*                           OS_ERR_NONE             The call was successful and the mutex was signaled.
*                           OS_ERR_MUTEX_NESTING    Mutex owner nested its use of the mutex
*                           OS_ERR_MUTEX_NOT_OWNER  If the task posting is not the Mutex owner
*                           OS_ERR_OBJ_PTR_NULL     If 'p_mutex' is a NULL pointer.
*                           OS_ERR_OBJ_TYPE         If 'p_mutex' is not pointing at a mutex
*                           OS_ERR_POST_ISR         If you attempted to post from an ISR
*
* Returns    : none
************************************************************************************************************************
*/
void OSResourceMutexPost(OS_MUTEX *p_mutex, OS_OPT opt, OS_ERR *p_err){
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();
    int oldCeiling;
    avl_node* minNode;
    avl_linkedlist* listNode;
    OS_TCB* toReleaseTCB;
    int i, listSize;
    int blockingPrio;

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif


    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */

        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }

    OS_CRITICAL_ENTER();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
    p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;
    OS_CRITICAL_EXIT();
    
    if (p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0 ) {     /* Are we done with all nestings?                         */                                                 
        *p_err = OS_ERR_MUTEX_NESTING;
        return;
    }
    //update the resource count for the task
    (OSTCBCurPtr->resourceCount)--;
//    printf("resource count = %d\n",OSTCBCurPtr -> resourceCount);
    blockingPrio = peekPrio(&OSResourceStack);
    //update the system ceiling and pop the resource from the resource stack
    pop(&OSResourceStack);
    OS_System_Ceiling = peek(&OSResourceStack);
//    printf("CEILING = %d\n", OS_System_Ceiling);
//    printf("min = %d\n", OSResourceTree.min);
    //unblock the tasks whose priority is higher than the updated system ceiling 
    if(OS_System_Ceiling > 0){
      while(OSResourceTree.min < OS_System_Ceiling && OSResourceTree.min != 0){
  //      printf("GOES IN\n");
        minNode = avl_tree_remove_min(&OSResourceTree);
        listNode = &(minNode -> ll);
        listSize = listNode -> size;
        for(i = 0 ; i <= listSize-1 ; i++){
  //        printf("GOES IN2\n");
          toReleaseTCB = listNode -> head -> value;

          OS_CRITICAL_ENTER();
          //release the task to the ready structure
          heap_insert(&OSRdyMinHeap, toReleaseTCB -> Prio, toReleaseTCB);
          //remove the task from the list
          removeResourceListNode(listNode, 0);
          //change the state of the task to ready
          toReleaseTCB -> TaskState  = OS_TASK_STATE_RDY;
          OS_CRITICAL_EXIT();
        }
        //release the partition for the avl tree node
        release_avl_node(minNode);
      }
    }
    else if(OS_System_Ceiling == 0){
      //release all the blocked tasks in the tree
      while(OSResourceTree.min != 0){
//      printf("GOES IN\n");
        flagl = 1;
        
        minNode = avl_tree_remove_min(&OSResourceTree);
        listNode = &(minNode -> ll);
        listSize = listNode -> size;
        for(i = 0 ; i <= listSize-1 ; i++){
//        printf("GOES IN2\n");
          toReleaseTCB = listNode -> head -> value;
          
          OS_CRITICAL_ENTER();
          //release the task to the ready structure
          toReleaseTCB -> TaskState  = OS_TASK_STATE_RDY;
          heap_insert(&OSRdyMinHeap, toReleaseTCB -> Prio, toReleaseTCB);
          
          //remove the task from the list
          removeResourceListNode(listNode, 0);

          OS_CRITICAL_EXIT();
        }
        //release the partition for the avl tree node
        release_avl_node(minNode);
      }
    }
    
    if (OSTCBCurPtr -> resourceCount == 0 && OSTCBCurPtr->Prio != OSTCBCurPtr->originalPrio) {
        /* Remove from ready list at current priority             */
//        printf("RESTORE\n");
        OS_CRITICAL_ENTER();
//        OSSchedLock (p_err);
        removeTCBHeap(&OSRdyMinHeap, OSTCBCurPtr -> Prio, OSTCBCurPtr);                   
//	OS_RdyListRemove(OSTCBCurPtr);
        /* Lower owner's priority back to its original one        */
	OSTCBCurPtr->Prio = p_mutex->OwnerOriginalPrio;     
        /* Insert owner in ready list at new priority             */
        heap_insert(&OSRdyMinHeap, OSTCBCurPtr -> Prio, OSTCBCurPtr);
//	OS_PrioInsert(OSTCBCurPtr->Prio);
//	OS_RdyListInsertTail(OSTCBCurPtr);                  
	OSPrioCur = OSTCBCurPtr->Prio;
//        OSSchedUnlock (p_err);
        OS_CRITICAL_EXIT();
    }
    
    /* Run the scheduler                                      */
    OSSched();                                          

    *p_err = OS_ERR_NONE;
}
